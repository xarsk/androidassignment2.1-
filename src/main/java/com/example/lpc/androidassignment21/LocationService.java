package com.example.lpc.androidassignment21;

import android.Manifest;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

public class LocationService extends Service {
    private LocationManager locationManager;
    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {

            Date timestamp = new Date(System.currentTimeMillis());
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-YYYY HH:mm:ss");
            String dt = format.format(timestamp);

            Double lat = location.getLatitude();
            Double lon = location.getLongitude();

            System.out.println("Latitude: " + lat + " Longitude: " + lon);
            //Like adding a query via database itself
            ContentValues values = new ContentValues();
            values.put("USERID", MainActivity.userID);
            values.put("LATITUDE", lat);
            values.put("LONGITUDE", lon);
            values.put("DT", dt);
            //Hitting it to the uri
            String AUTHORITY = "com.example.lpc.assignment1";
            String PATH = "/gps";
            Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + PATH);

            Uri returnUri = getContentResolver().insert(CONTENT_URI, values);
            if (returnUri != null) {
                Toast.makeText(LocationService.this, "Location registered", Toast.LENGTH_SHORT).show();
                System.out.println("Location registered");
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };


    public void startListening() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }//Permission checking for fine and coarse location

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 20, locationListener);
        //Trying to use the time and distance params that location manager provides

    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("You don't need this");
    }

    @Override
    public void onCreate() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        System.out.println("Location Service Triggered");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startListening();
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        locationManager.removeUpdates(locationListener);
    }
}