package com.example.lpc.androidassignment21;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.widget.Toast;

public class MyBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "Airplane event triggered", Toast.LENGTH_LONG).show();
        try {
            intent.setClass(context, Class.forName(LocationService.class.getName()));
            boolean mode = airplaneModeStatus(context);
            if(mode) { //Airplane mode is on
                context.startService(intent);
                System.out.println("Airplane mode ON");
            } else { //Airplane mode is off
                context.stopService(intent);
                System.out.println("Airplane mode turned OFF");
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static boolean airplaneModeStatus(Context context) {
            return Settings.System.getInt(context.getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) != 0;

    }
}