package com.example.lpc.androidassignment21;

import android.Manifest;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private MyBroadcastReceiver receiver;
    private IntentFilter filter;
    public static String userID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        receiver = new MyBroadcastReceiver();
        filter = new IntentFilter();
        filter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        startListening();
        //On create, check for airplane mode change, and start listening

    }

    private void startListening() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 7);
            return;
        }//Permission checking for fine and coarse location

        Button buttonAdd = findViewById(R.id.buttonAdd);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editTextID = findViewById(R.id.editTextID);
                userID = editTextID.getText().toString();

                if (userID.trim().length() == 0) { //NullChecking
                    Toast.makeText(MainActivity.this, "Specify User ID", Toast.LENGTH_SHORT).show();
                } else {
                    registerReceiver(receiver, filter);
                    Toast.makeText(MainActivity.this, "Now change the airplane mode", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}